require('dotenv/config');
const express = require('express');
const app = express();
const port = process.env.PORT;
const Discord = require('discord.js');
const cron = require('node-cron');
const axios = require('axios');

//Idiomatic expression in express to route and respond to a client request
app.get('/', (req, res) => { 
    res.send("Bot Active");
});

app.listen(port, () => {
    console.log(`Now listening on port ${port}`); 
});

const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] });

var date = new Date();
var hour = date.getHours();
var minute = date.getMinutes();

client.on("ready", () => {
  //check bot active
  console.log(`Logged in as ${client.user.tag} at ${hour}:${minute}`)
  //get channel and send message
  client.channels.fetch(process.env.ID_SERVER).then(channel => {
    //set message with cron
    
    cron.schedule('0 8,17 * * 1-5', function() {
      console.log(`running a task at ${new Date().getHours()}:${new Date().getMinutes()}`);
      console.log(`${new Date().toISOString().slice(0, 10)}`);
      //channel.send(process.env.MESSAGE);
      //call API 
      axios.get(`${process.env.URL}/batch/holiday`).then(resp => {
        //console.log(resp.data);
        const holidayList = resp.data;
        if(holidayList.includes(new Date().toISOString().slice(0, 10)) === false){ // check today is not holiday
          //send message
          channel.send(process.env.MESSAGE);
        } 
      });
    });
  })
});

client.login(process.env.TOKEN);